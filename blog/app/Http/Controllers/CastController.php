<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class CastController extends Controller
{
    public function create(){
        $cast = DB::table('cast')->get();
        return view('cast.create', compact('cast'));
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $cast = new cast;
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;

        $cast->save();

    return redirect('/cast');  
    }
    public function index(){
        $cast = Cast::all();
        return view ('cast.index', compact ('cast'));
    }
    public function show($id){
        $cast = Cast::findOrFail($id);

        return view('cast.show', compact('cast'));

    }
    public function edit($id){
        $cast = Cast::findOrFail($id);


        return view('cast.edit', compact('cast'));

    }
    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $cast = Cast::findOrFail($id);
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->update();
        return redirect ('/cast');
        
 
    }

    public function destroy($id){
        $cast = Cast::findOrFail($id);
        $cast->delete();
        return redirect('/cast');
    }
}
