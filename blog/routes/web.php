<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@auth');
Route::post('/welcome', 'AuthController@kirim');

Route::get('/data-table', function(){
    return view('table.datatable');
});
Route::get('/table', function(){
    return view('table.table');
});


// Route::get('/cast/create','CastController@create');
//Route::post('/cast','CastController@store');
//Route::get('/cast', 'CastController@index');
//Route::get('/cast/{cast_id}', 'CastController@show');
//Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//Route::put('/cast/{cast_id}', 'CastController@update');
//Route::delete('/cast/{cast_id}', 'CastController@destroy'); /


//CRUD GAME
Route::get('/game/create','gameController@create');
Route::post('/game','gameController@store');
Route::get('/game', 'gameController@index');
Route::get('/game/{game_id}', 'gameController@show');
Route::get('/game/{game_id}/edit', 'gameController@edit');
Route::put('/game/{game_id}', 'gameController@update');
Route::delete('/game/{game_id}', 'gameController@destroy');




//CRUD Equilent Cast
Route::resource('cast','CastController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
